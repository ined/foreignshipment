﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ForeighShipment
{
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute("documents", Namespace = "", IsNullable = false)]
    public class Documents
    {
        [XmlAttributeAttribute("version")]
        public string Version;

        [XmlElementAttribute("foreign_shipment")]
        public ForeignShipment[] ForeignShipments;
    }


    [XmlTypeAttribute(AnonymousType = true, Namespace = "")]
    public class ForeignShipment
    {
        [System.Xml.Serialization.XmlAttributeAttribute("action_id")]
        public int ActionId;

        [XmlElementAttribute("subject_id")]
        public string SubjectId;

        [XmlElementAttribute("seller_id")]
        public string SellerId;

        [XmlElementAttribute("receiver_id")]
        public string ReceiverId;

        [XmlElementAttribute("operation_date")]
        public string OperationDate;

        [XmlElementAttribute("doc_num")]
        public int DocNum;

        [XmlElementAttribute("doc_date")]
        public string DocDate;

        [XmlArrayAttribute("order_details")]
        public Detail[] OrderDetails;

        [XmlElementAttribute("order_number")]
        public long OrderNumber;

        [XmlArrayAttribute("invoice_details")]
        public Union[] InvoiceDetails;
    }

    [XmlTypeAttribute("detail", Namespace = "")]
    public class Detail
    {
        [XmlElementAttribute("sscc")]
        public string Sscc;

        [XmlArrayItemAttribute("sgtin")]
        [XmlArrayAttribute("content")]
        public string[] Content { get; set; }
    }

    [XmlTypeAttribute("union", Namespace = "")]
    public class Union
    {
        [XmlElementAttribute("invoice_number")]
        public string InvoiceNumber;

        [XmlElementAttribute("Desc_of_goods")]
        public string DescOfGoods;

        [XmlElementAttribute("gtin")]
        public string Gtin;

        [XmlElementAttribute("Quantity")]
        public int Quantity;

        [XmlElementAttribute("tnved_code")]
        public string TnvedCode;
    }

    class Program
    {
        public Documents ForeighShipments { get; set; }

        static void Main(string[] args)
        {
            Program program = new Program();

            // Выполняем десериализацию.
            program.ReadDopcuments("ForeignShipment.xml");

            // Проверяем результат .
            program.CreateDopcuments("ForeignShipmentNew.xml");

        }

        // Считываем из файла XML-документ и десериализируем
        protected void ReadDopcuments(string filename)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Documents));
                FileStream fs = new FileStream(filename, FileMode.Open);
                ForeighShipments = (Documents)serializer.Deserialize(fs);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }
        }

        // Сериализируем объект и записываем результат в файл
        private void CreateDopcuments(string filename)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Documents));
                TextWriter writer = new StreamWriter(filename);
                serializer.Serialize(writer, ForeighShipments);
                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }
        }

    }
}
